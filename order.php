<?php
require_once('koneksi.php');

if (isset($_POST['submit'])) {
    if ($_POST['jumlah'] > 0) {
        $nama = $_POST['firstName'] + ' ' + $_POST['lastName'];
        $produk = $_POST['produk'];
        $warna = $_POST['warna'];
        $ukuran = $_POST['ukuran'];
        $jumlah = $_POST['jumlah'];
        $tanggal = date('d-m-Y H:i:s');
        $alamat = $_POST['alamat'];
        $telepon = $_POST['telepon'];

        // $insertCustomer = mysqli_query($con, "INSERT INTO tb_customer('nama', 'alamat', 'telp') VALUES ('$nama', '$alamat', '$telepon')");

        // $insertJual = mysqli_query($con, "INSERT INTO tb_jual(tanggal, idcustomer, biaya, diskon) VALUES ('$tanggal', '')");
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ApaanSihKak's Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>

    <script>
        window.onload = function() {
            var typed = new Typed('.typing-effect', {
                strings: [
                    "Let me tell you, ^700 a hero tells no lie.",
                    "I am a hero ^400 for tomorrow.",
                    "Many do not know that ^400.^400.^400.",
                    "I code HTML, ^400 CSS, ^400 Java, ^400 Kotlin, ^400 Dart, ^400 and JS just like you.",
                    "... :p"
                ],
                typeSpeed: 50,
                backDelay: 1500,
                loop: 1
            });
        }
    </script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="user.php"> User </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="container" style="margin-top:20px">
        <h2 align="center">Silahkan pilih yang Anda mau...</h2>
        <hr>
        <br>


        <form action="" method="POST">

            <div class="form-group row">
                <div class="form-group col-md-6">
                    <label for="firstName">First Name</label>
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="willy" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="lastName">Last Name</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="sianturi" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Jln. ..., Gg. ... No. ..., Medan" required>
            </div>

            <div class="form-group row">
                <label for="alamat">No. Telepon</label>
                <input type="text" class="form-control" id="telepon" name="telepon" placeholder="+62 xxxxxxxxxx" required>
            </div>

            <div class="form-group row">
                <label for="produk">Produk</label>
                <select class="form-control" id="produk" name="produk">
                    <option value="baju">Kemeja</option>
                    <option value="kaos">Kaos</option>
                    <option value="kaos">Celana</option>
                    <option value="gaun">Gaun</option>
                    <option value="rok">Rok</option>
                    <option value="dasi">Dasi</option>
                </select>
            </div>

            <div class="form-group row">
                <label for="warna">Warna</label>
                <select class="form-control" id="warna" name="warna">
                    <option value="hitam">Hitam</option>
                    <option value="putih">Putih</option>
                    <option value="merah">Merah</option>
                    <option value="biru">Biru</option>
                </select>
            </div>

            <div class="form-group row">
                <label for="ukuran">Ukuran</label>
                <select class="form-control" id="ukuran" name="ukuran">
                    <option value="XS">XS</option>
                    <option value="S">S</option>
                    <option value="M">M</option>
                    <option value="L">L</option>
                    <option value="XL">XL</option>
                </select>
            </div>

            <div class="form-group row">
                <div class="form-group col-md-6" style="text-align: right">
                    <label for="jumlah">Jumlah</label>
                </div>
                <div class="form-group col-md-6">
                    <input type="number" name="jumlah" class="form-control" required />
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm">
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </div>
            </div>

        </form>

        <marquee>
            <p>Contact information: <a href="">1945801-SIA17</a></p>
        </marquee>
    </div>


</body>

<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>