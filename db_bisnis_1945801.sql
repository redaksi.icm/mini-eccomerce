-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2020 at 04:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bisnis_1945801`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_beli`
--

CREATE TABLE `tb_beli` (
  `id` int(11) NOT NULL,
  `tanggal` varchar(128) NOT NULL,
  `idsupplier` int(11) NOT NULL,
  `iddetailproduk` int(11) NOT NULL,
  `qty` tinyint(3) UNSIGNED NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `biaya` decimal(10,0) NOT NULL,
  `diskon` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_beli`
--

INSERT INTO `tb_beli` (`id`, `tanggal`, `idsupplier`, `iddetailproduk`, `qty`, `harga`, `biaya`, `diskon`, `total`) VALUES
(1, '2020-06-20', 1, 1, 1, '10000', '1000', '1500', '9500'),
(2, '2020-06-21', 1, 4, 2, '15000', '1000', '4500', '26500'),
(4, '2020-07-05', 1, 4, 2, '17500', '1000', '1500', '34500'),
(6, '2020-07-05', 4, 6, 3, '20000', '1000', '750', '60250');

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `telp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detailbeli`
--

CREATE TABLE `tb_detailbeli` (
  `id` int(11) NOT NULL,
  `idbeli` int(11) NOT NULL,
  `iddetailproduk` int(11) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `qty` tinyint(3) UNSIGNED NOT NULL,
  `subtotal` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detailjual`
--

CREATE TABLE `tb_detailjual` (
  `id` int(11) NOT NULL,
  `idjual` int(11) NOT NULL,
  `iddetailproduk` int(11) NOT NULL,
  `qty` tinyint(3) UNSIGNED NOT NULL,
  `subtotal` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detailproduk`
--

CREATE TABLE `tb_detailproduk` (
  `id` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `idwarna` int(11) NOT NULL,
  `idukuran` int(11) NOT NULL,
  `stok` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_detailproduk`
--

INSERT INTO `tb_detailproduk` (`id`, `idproduk`, `idwarna`, `idukuran`, `stok`) VALUES
(1, 1, 1, 1, 14),
(4, 2, 3, 3, 13),
(5, 1, 1, 3, 5),
(6, 6, 2, 5, 23);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jual`
--

CREATE TABLE `tb_jual` (
  `id` int(11) NOT NULL,
  `tanggal` varchar(128) NOT NULL,
  `iduser` int(11) NOT NULL,
  `iddetailproduk` int(11) NOT NULL,
  `qty` tinyint(4) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `biaya` decimal(10,0) NOT NULL,
  `diskon` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `harga` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id`, `nama`, `harga`) VALUES
(1, 'Kopi Mantap', '25000'),
(2, 'Rokok on Fire', '17500'),
(3, 'Nasi Putih edited', '7500'),
(6, 'Rokok Santuy', '17500'),
(7, 'Ngemil Enak', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `telp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id`, `nama`, `alamat`, `telp`) VALUES
(1, 'Indofood edited', 'Jalan jalan edited', '08555123748'),
(2, 'PT. Digitalent Scholarship', 'Kementerian Komunikasi dan Informatika RI Jl. Medan Merdeka Barat No. 9 Jakarta Pusat, 10110', 'digitalent[at]mail[dot]kominfo[dot]go[dot]id'),
(4, 'PT. Maju Bersama', 'Jln. KL. Yos Sudarso No. 123-125 Glugur Darat II, Medan Sumatra Utara 20238', '061 - 6628120');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ukuran`
--

CREATE TABLE `tb_ukuran` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_ukuran`
--

INSERT INTO `tb_ukuran` (`id`, `nama`) VALUES
(1, 'XS'),
(2, 'S'),
(3, 'M'),
(4, 'L'),
(5, 'XL');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `isadmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `alamat`, `telp`, `email`, `password`, `isadmin`) VALUES
(1, 'asd', 'asdasdasd', '08123617312', 'asd@asd.asd', '$2y$10$1XMvd0TPYqsQsBib3byo3.r09zeYFSm4sw//ED26uIt40H4h6JEuy', 0),
(2, 'qwe', 'qweqweqwe', '081236173129', 'qwe@qwe.qwe', '$2y$10$3O3WFzKkAcUEqEeRLQo3jOdWSyXUVUf1l5z1lvtuGae7l1watYNru', 0),
(3, 'admin', 'adminadminadmin', '085208120822', 'admin@admin.admin', '$2y$10$rcMyr6fkqb400XTjXKjHAObuut3L25JRSZztnTnZUYTZ/A7PE/veu', 1),
(4, 'Cecilion Carmila', 'Land of Dawn', '082208210813', 'cecilioncarmila@gmail.com', '$2y$10$LizNRQnya5/MY1KyGzz8L.maCvweqANvJbA34YvqKNkBLx6bvQN0i', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_warna`
--

CREATE TABLE `tb_warna` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_warna`
--

INSERT INTO `tb_warna` (`id`, `nama`) VALUES
(1, 'Hitam'),
(2, 'Putih'),
(3, 'Merah'),
(4, 'Biru'),
(5, 'Kuning');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_beli`
--
ALTER TABLE `tb_beli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detailbeli`
--
ALTER TABLE `tb_detailbeli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detailjual`
--
ALTER TABLE `tb_detailjual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detailproduk`
--
ALTER TABLE `tb_detailproduk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jual`
--
ALTER TABLE `tb_jual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_ukuran`
--
ALTER TABLE `tb_ukuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_warna`
--
ALTER TABLE `tb_warna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_beli`
--
ALTER TABLE `tb_beli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detailbeli`
--
ALTER TABLE `tb_detailbeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detailjual`
--
ALTER TABLE `tb_detailjual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detailproduk`
--
ALTER TABLE `tb_detailproduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_jual`
--
ALTER TABLE `tb_jual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_ukuran`
--
ALTER TABLE `tb_ukuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_warna`
--
ALTER TABLE `tb_warna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
