<table width="100%" border="0" align="center" cellpadding="8">
	<tr align="center">
		<td><a href="home.php">Home</a></td>
		<?php
		if ($_SESSION['user']['isadmin'] == 1) // jika admin
		{
		?>
			<td><a href="user.php">User</a></td>
			<td><a href="lihat_supplier.php">Supplier</a></td>
			<td><a href="lihat_warna.php">Warna</a></td>
			<td><a href="lihat_ukuran.php">Ukuran</a></td>
			<td><a href="lihat_produk.php">Produk</a></td>
			<td><a href="lihat_detailproduk.php">Detail Produk</a></td>
			<td><a href="lihat_beli.php">Pembelian</a></td>
			<!-- NAVBAR -->
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
				<a class="navbar-brand" href="">ApaanSihKak's Shop</a>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="user.php"> User </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="lihat_supplier.php"> Supplier </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="lihat_beli.php"> Pembelian </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="order.php"> Order </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="warna.php"> Warna </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="ukuran.php"> Ukuran </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="lihat_produk.php"> Produk </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="about.php"> About Me </a>
					</li>
				</ul>
			</nav>
		<?php
		}
		?>
		<td><a href="lihat_jual.php">Penjualan</a></td>
	</tr>
</table>