<?php
require_once('koneksi.php');
require_once('authen.php');

if (isset($_POST['logout'])) {
	session_destroy();
	header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ApaanSihKak's SHOP | Home</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>
</head>

<body>
	<?php
	include_once('menu.php');
	?>
	<h3>SELAMAT DATANG <?php echo strtoupper($_SESSION['user']['nama']); ?></h3>

	<form action="" method="POST">
		<input type="submit" name="logout" value="Logout">
	</form>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

</body>

<footer class="sticky-footer bg-white" style="position: absolute; bottom: 0; width: 100%; height: 30px;">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
		</div>
	</div>
</footer>

</html>