<?php
require_once('koneksi.php');

if (isset($_POST['register'])) {

	// filter data yang di-inputkan
	$nama = filter_input(INPUT_POST, 'nama', FILTER_SANITIZE_STRING);
	$alamat = filter_input(INPUT_POST, 'alamat', FILTER_SANITIZE_STRING);
	$telp = filter_input(INPUT_POST, 'telp', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
	// enkripsi password
	$password = password_hash($_POST['password1'], PASSWORD_DEFAULT);
	// $password2 = password_hash($_POST['password2'], PASSWORD_DEFAULT);

	// menyiapkan query
	$sql = "INSERT INTO tb_user (nama, alamat, telp, email, password, isadmin) VALUES (:nama, :alamat, :telp, :email, :password, :isadmin)";
	$stmt = $db->prepare($sql);

	// bind parameter ke query
	$params = array(
		":nama" => $nama,
		":alamat" => $alamat,
		":telp" => $telp,
		":email" => $email,
		":password" => $password,
		":isadmin" => 0
	);

	// eksekusi query untuk menyimpan ke database
	$saved = $stmt->execute($params);

	// jika query simpan berhasil, maka user sudah terdaftar
	// maka alihkan ke halaman login
	if ($saved) header("Location: login.php");
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ApaanSihKak's SHOP | Registration Page</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
	<div class="register-box">
		<div class="register-logo">
			<a href="AdminLTE/index2.html"><b>ApaanSihKak's </b>SHOP</a>
		</div>

		<div class="card">
			<div class="card-body register-card-body">
				<p class="login-box-msg">Register a new membership</p>

				<form action="" method="post">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Nama" name="nama">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-user"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Alamat" name="alamat">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fa fa-map-marker"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Telepon" name="telp">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fa fa-phone"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="email" class="form-control" placeholder="Email" name="email">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-envelope"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" class="form-control" placeholder="Password" name="password1">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" class="form-control" placeholder="Ulangi password" name="password2">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							<div class="icheck-primary">
								<input type="checkbox" id="agreeTerms" name="terms" value="agree">
								<label for="agreeTerms">
									I agree to the <a href="#">terms</a>
								</label>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block" name="register">Register</button>
						</div>
						<!-- /.col -->
					</div>
				</form>

				<a href="login.php" class="text-center">I already have a membership</a>
			</div>
			<!-- /.form-box -->
		</div><!-- /.card -->
	</div>
	<!-- /.register-box -->

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

	<script src="fontawesome/js/all.min.js"></script>
	<script src="fontawesome/js/fontawesome.min.js"></script>
</body>

</html>