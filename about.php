<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ApaanSihKak's Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>

    <script>
        window.onload = function() {
            var typed = new Typed('.typing-effect', {
                strings: [
                    "Let me tell you, ^700 a hero tells no lie.",
                    "I am a hero ^400 for tomorrow.",
                    "Many do not know that ^400.^400.^400.",
                    "I code HTML, ^400 CSS, ^400 Java, ^400 Kotlin, ^400 Dart, ^400 and JS just like you.",
                    "... :p"
                ],
                typeSpeed: 50,
                backDelay: 1500,
                loop: 1
            });
        }
    </script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="cart.php"> Cart </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-3 offset-md-1">
                    <img class="rounded-circle w-50 mx-auto d-block" src="images/profile.jpg"> </div>
                <div class="col-md-8">
                    <h1>Tumbur M. T. Sianturi</h1>
                    <h2><span class="typing-effect"></span></h2>
                    <p>Well, I am a hero of finggers and working as a software
                        developer.</p>
                </div>
            </div>
        </div>
    </div>

</body>


<footer class="sticky-footer bg-white">
    <marquee behavior="" direction="">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
            </div>
        </div>
    </marquee>
</footer>


</html>