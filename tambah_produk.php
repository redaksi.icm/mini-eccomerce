<?php
require_once('koneksi.php');


// $selectWarna = mysqli_query($con, "SELECT * FROM tb_warna");
// $rowWarna = mysqli_fetch_assoc($selectWarna);
// $namaWarna = $rowWarna["nama"];
// // var_dump($selectWarna[]);

// $selectUkuran = mysqli_query($con, "SELECT * FROM tb_ukuran");
// $rowUkuran = mysqli_fetch_array($selectUkuran);
// $namaUkuran = $rowUkuran["nama"];
// // var_dump($namaUkuran);


// if (isset($_POST['submit'])) {
//     $produk = $_POST['produk'];
//     $harga = $_POST['harga'];
//     $jumlah = $_POST['jumlah'];
//     $warna = $_POST['warna'];
//     $ukuran = $_POST['ukuran'];
// }

function tampilAlert($text, $tipe)
{
    echo "<div class=\"alert alert-" . $tipe . "\" role=\"alert\">
            <p>" . $text . "</p>
          </div>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ApaanSihKak's Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="user.php"> User </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_supplier.php"> Supplier </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="container" style="margin-top:20px">
        <h2 align="center">Tambahkan Produk...</h2>
        <hr>
        <br>


        <form action="" method="POST">

            <div class="form-group row">
                <label for="produk" class="col-md-2 col-form-label">Produk</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="produk" name="produk" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="harga" name="harga" required>
                </div>
            </div>

            <?php
            if (isset($_POST['submit'])) {
                $produk = $_POST['produk'];
                $harga = $_POST['harga'];

                $insertProduk = mysqli_query($con, "INSERT INTO tb_produk(nama, harga) VALUES ('$produk', '$harga')");

                if ($insertProduk) {
                    tampilAlert("Produk berhasil ditambahkan..", "success");
                } else {
                    tampilAlert("Produk gagal ditambahkan...", "danger");
                };
            }

            ?>

            <!-- <div class="invalid-feedback">
                Isi warna terlebih dahulu.
            </div> -->

            <div class="form-group row">
                <div class="col-md-2 offset-md-2">
                    <button type="submit" class="btn btn-primary btn-block" name="submit" value="submit">Submit</button>
                </div>
            </div>

        </form>

        <marquee>
            <p>Contact information: <a href="">1945801-SIA17</a></p>
        </marquee>
    </div>

    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>

</body>

<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>