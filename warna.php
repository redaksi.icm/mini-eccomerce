<?php
require_once('koneksi.php');

function tampilAlert($text, $tipe)
{
    echo "<div class=\"alert alert-" . $tipe . "\" role=\"alert\">
            <p>" . $text . "</p>
          </div>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ApaanSihKak's Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="cart.php"> Cart </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="container" style="margin-top:20px">
        <h2 align="center">Tambahkan Warna...</h2>
        <hr>
        <br>


        <form action="" method="POST">

            <div class="form-group row">
                <label for="warna">Warna</label>
                <input type="text" class="form-control" id="warna" name="warna" placeholder="Merah..." required>
            </div>

            <?php
            if (isset($_POST['submit'])) {
                $warna = $_POST['warna'];

                $insertWarna = mysqli_query($con, "INSERT INTO tb_warna(nama) VALUES ('$warna')");

                if ($insertWarna) {
                    tampilAlert("Warna berhasil ditambahkan..", "success");
                } else {
                    tampilAlert("Warna gagal ditambahkan...", "danger");
                };
            }

            ?>

            <!-- <div class="invalid-feedback">
                Isi warna terlebih dahulu.
            </div> -->

            <div class="form-group row">
                <div class="col-md-4 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-block" name="submit" value="submit">Submit</button>
                </div>
            </div>

        </form>

        <marquee>
            <p>Contact information: <a href="">1945801-SIA17</a></p>
        </marquee>
    </div>


</body>

<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>