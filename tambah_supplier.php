<?php
include_once('koneksi.php');

// if (isset($_POST['tambah'])) {
// 	$nama = $_POST['nama'];
// 	$alamat = $_POST['alamat'];
// 	$telepon = $_POST['telepon'];

// 	$insert = mysqli_query($con, "INSERT INTO tb_supplier(nama, alamat, telp) VALUES ('$nama', '$alamat', '$telepon')");
// }

function tampilAlert($text, $tipe)
{
	echo "<div class=\"alert alert-" . $tipe . "\" role=\"alert\">
            <p>" . $text . "</p>
          </div>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ApaanSihKak's SHOP | Supplier</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>

	<div class="container-fluid">

		<!-- NAVBAR -->
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="user.php"> User </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="lihat_supplier.php"> Supplier </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="order.php"> Order </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="warna.php"> Warna </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="ukuran.php"> Ukuran </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="lihat_produk.php"> Produk </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="about.php"> About Me </a>
				</li>
			</ul>
		</nav>


		<h2 align="center"> Data Supplier</h2>
		<hr>
		<br>

		<div class="col-10 offset-1">

			<form action="" method="POST">
				<div class="form-group row">
					<label for="nama" class="col-sm-2 col-form-label"> Nama </label>
					<div class="col-sm-7">
						<input type="text" class="form-control" id="nama" name="nama" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="alamat" class="col-sm-2 col-form-label"> Alamat </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="alamat" name="alamat" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="telepon" class="col-sm-2 col-form-label"> Telepon </label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="telepon" name="telepon" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="tambah" class="col-sm-2 col-form-label"></label>
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary" name="tambah"> Tambah Supplier </button>
					</div>
				</div>
				<?php
				if (isset($_POST['tambah'])) {
					$nama = $_POST['nama'];
					$alamat = $_POST['alamat'];
					$telepon = $_POST['telepon'];

					$insert = mysqli_query($con, "INSERT INTO tb_supplier(nama, alamat, telp) VALUES ('$nama', '$alamat', '$telepon')");

					if ($insert) {
						tampilAlert("Supplier berhasil ditambahkan..", "success");
					} else {
						tampilAlert("Supplier gagal ditambahkan...", "danger");
					}
				}
				?>
			</form>
		</div>

	</div>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

	<script src="fontawesome/js/all.min.js"></script>
	<script src="fontawesome/js/fontawesome.min.js"></script>
</body>

<footer class="sticky-footer bg-white">
	<marquee behavior="" direction="">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
			</div>
		</div>
	</marquee>
</footer>

</html>