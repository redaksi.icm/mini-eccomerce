<?php
require_once('koneksi.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ApaanSihKak's Shop | Supplier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="user.php"> User </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_supplier.php"> Supplier </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="container" style="margin-top:20px">

        <h2 align="center"> Data Supplier</h2>
        <hr>
        <br>

        <div class="col-md-3 offset-md-11">
            <a href="tambah_supplier.php"><i class="fa fa-plus-square" aria-hidden="true"></i> Supplier</a> <br>
        </div>

        <form action="" method="post">
            <table class="table table-hover table-dark">
                <thead>
                    <tr align="center">
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Telepon</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <?php
                $select = mysqli_query($con, "SELECT * FROM tb_supplier");
                $i = 1;
                while ($row = mysqli_fetch_array($select)) {
                    echo "<tbody>";
                    echo "<tr align='center'>";
                    echo "<th scope='row'>" . $i++ . "</th>";

                    echo "<td> $row[nama] </td>";
                    echo "<td> $row[alamat] </td>";
                    echo "<td> $row[telp] </td>";

                    // aksi ubah|hapus
                    echo "<td><a href='ubah_supplier.php?id=$row[id]'> <i class='fa fa-edit'></i> </a> | <a href='hapus_supplier.php?id=$row[id]' onclick='return confirm(\"Anda yakin menghapus data ini?\")'> <i class='fa fa-trash'> </a></td>";
                    echo "</tr>";
                    echo "</tbody>";
                }
                ?>
        </form>

    </div>

    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>

    <!-- jQuery -->
    <script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="AdminLTE/dist/js/adminlte.min.js"></script>

    <script src="fontawesome/js/all.min.js"></script>
    <script src="fontawesome/js/fontawesome.min.js"></script>

</body>

<footer class="sticky-footer bg-white">
    <marquee behavior="" direction="">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
            </div>
        </div>
    </marquee>
</footer>

</html>