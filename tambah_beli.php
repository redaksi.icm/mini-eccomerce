<?php
require_once('koneksi.php');
session_start();

if (isset($_POST['submit'])) {
	$tanggal = $_POST['tanggal'];
	$idsupplier = $_POST['idsupplier'];
	$iddetailproduk = $_POST['iddetailproduk'];
	$qty = $_POST['qty'];
	$harga = $_POST['harga'];
	$biaya = $_POST['biaya'];
	$diskon = $_POST['diskon'];
	$total = ($qty * $harga) + $biaya - $diskon;

	$insert = mysqli_query($con, "INSERT INTO tb_beli(tanggal, idsupplier, iddetailproduk, qty, harga, biaya, diskon, total) VALUES ('$tanggal', $idsupplier, $iddetailproduk, $qty, $harga, $biaya, $diskon, $total)");

	if ($insert) {
		$update_stok = mysqli_query($con, "UPDATE tb_detailproduk SET stok = (stok + $qty) WHERE id = $iddetailproduk");
		echo "<font color=green> Data Berhasil Ditambah </font>";
	} else {
		echo "<font color=red> Data Gagal Ditambah </font>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ApaanSihKak's SHOP | Pembelian</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>
	<?php
	include_once('menu.php');
	?>
	<h3>Tambah Pembelian</h3>
	<a href="lihat_beli.php">Kembali</a>
	<form action="" method="POST">
		<table width="300" border="0">
			<tr>
				<td>Tanggal</td>
				<td><input type="date" name="tanggal" id="tanggal" required></td>
			</tr>
			<tr>
				<td>Supplier</td>
				<td>
					<select name="idsupplier" id="idsupplier">
						<?php
						$select_supplier = mysqli_query($con, "SELECT * FROM tb_supplier");
						while ($row_supplier = mysqli_fetch_array($select_supplier)) {
							echo "<option value=$row_supplier[id]> $row_supplier[nama] </option>";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Produk</td>
				<td>
					<select name="iddetailproduk" id="iddetailproduk">
						<?php
						$select_detailproduk = mysqli_query($con, "SELECT * FROM tb_detailproduk");
						while ($row_detailproduk = mysqli_fetch_array($select_detailproduk)) {
							$select_produk = mysqli_query($con, "SELECT * FROM tb_produk WHERE id = $row_detailproduk[idproduk]");
							$row_produk = mysqli_fetch_array($select_produk);

							$select_warna = mysqli_query($con, "SELECT * FROM tb_warna WHERE id = $row_detailproduk[idwarna]");
							$row_warna = mysqli_fetch_array($select_warna);

							$select_ukuran = mysqli_query($con, "SELECT * FROM tb_ukuran WHERE id = $row_detailproduk[idukuran]");
							$row_ukuran = mysqli_fetch_array($select_ukuran);

							echo "<option value=$row_detailproduk[id]>" . $row_produk['nama'] . " - " . $row_warna['nama'] . " - " . $row_ukuran['nama'] . "</option>";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Qty</td>
				<td><input type="number" name="qty" id="qty" min="1" value="1" required /></td>
			</tr>
			<tr>
				<td>Harga</td>
				<td><input type="number" name="harga" id="harga" min="0" value="0" required /></td>
			</tr>
			<tr>
				<td>Biaya</td>
				<td><input type="number" name="biaya" id="biaya" min="0" value="0" required /></td>
			</tr>
			<tr>
				<td>Diskon</td>
				<td><input type="number" name="diskon" id="diskon" min="0" value="0" required /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" id="qty" value="Submit"></td>
			</tr>
		</table>
	</form>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

	<script src="fontawesome/js/all.min.js"></script>
	<script src="fontawesome/js/fontawesome.min.js"></script>
</body>

<footer class="sticky-footer bg-white">
	<marquee behavior="" direction="">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
			</div>
		</div>
	</marquee>
</footer>

</html>