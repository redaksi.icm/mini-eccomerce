<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
    $select = mysqli_query($con, "SELECT * FROM tb_supplier WHERE id = " . $_GET['id']);
    $row = mysqli_fetch_array($select);
    $nama = $row["nama"];
    $alamat = $row["alamat"];
    $telepon = $row["telp"];

    // $id = $_GET['id'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ApaanSihKak's Shop | Supplier</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="home.php">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="user.php"> User </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_supplier.php"> Supplier </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="col-md-12 offset-md-3">
        <div class="col-6">
            <center>
                <h1>Ubah Supplier</h1>
            </center>
            <hr>
        </div>
        <br>

        <div class="container">
            <div class="row">
                <div class="col-md-3 offset-md-5">
                    <a href="lihat_supplier.php">Kembali</a>
                </div>
            </div>
        </div>

        <br>

        <form name="form1" method="POST" action="">

            <!-- <div class="form-group row">
                <label for="id" class="col-sm-2 col-form-label"> id Produk </label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="id" value="<?= $id; ?>" required>
                </div>
            </div> -->

            <input type="hidden" value="<?= $_GET['id']; ?>" name="id">

            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label"> Nama Supplier </label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="nama" value="<?= $nama; ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat" class="col-sm-2 col-form-label"> Alamat </label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="alamat" value="<?= $alamat; ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="telepon" class="col-sm-2 col-form-label"> Telepon </label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="telepon" value="<?= $telepon; ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-1">
                    <button class="btn btn-primary" type="submit" name="submit">Ubah</button>
                </div>
            </div>

            <div class="col-6">
                <?php
                if (isset($_POST['submit'])) {
                    $nama = $_POST["nama"];
                    $alamat = $_POST["alamat"];
                    $telepon = $_POST["telepon"];

                    $id = $_POST["id"];

                    function tampilAlert($text, $tipe)
                    {
                        echo "<div class=\"alert alert-" . $tipe . "\" role=\"alert\">
                                    <p>" . $text . "</p>
                                </div>";
                    }

                    $update = mysqli_query($con, "UPDATE tb_supplier SET nama = '$nama', alamat = '$alamat', telp = '$telepon' WHERE id = '$id'");
                    if ($update) {
                        tampilAlert("Supplier berhasil diubah..", "success");
                    } else {
                        tampilAlert("Supplier gagal diubah...", "danger");
                    }
                }
                ?>
            </div>

        </form>
    </div>

    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>

    <!-- jQuery -->
    <script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="AdminLTE/dist/js/adminlte.min.js"></script>

    <script src="fontawesome/js/all.min.js"></script>
    <script src="fontawesome/js/fontawesome.min.js"></script>
</body>

<footer class="sticky-footer bg-white" style="position: absolute; bottom: 0; width: 100%; height: 30px;">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>