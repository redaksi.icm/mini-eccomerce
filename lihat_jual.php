<?php
require_once('koneksi.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ApaanSihKak's SHOP | Penjualan</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="SIA17_1945801/fontawesome/css/all.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>
	<?php
	include_once('menu.php');
	?>
	<h3>Penjualan</h3>
	<div class="col-md-3 offset-md-11">
		<a href="tambah_jual.php"><i class="fa fa-plus-square" aria-hidden="true"></i> Penjualan</a> <br>
	</div>

	<?php
	$select_jual = mysqli_query($con, "SELECT * FROM tb_jual");
	echo "Jumlah Data = " . mysqli_num_rows($select_jual) . "<br><br>";
	?>
	<table border="1" width="100%" cellpadding="5" cellspacing"0">
		<tr align="center">
			<th>No.</th>
			<th>Tanggal</th>
			<th>Nama</th>
			<th>Detail Produk</th>
			<th>Qty</th>
			<th>Harga</th>
			<th>Biaya</th>
			<th>Diskon</th>
			<th>Total</th>
			<th>Aksi</th>
		</tr>
		<?php
		$i = 1;
		while ($row_jual = mysqli_fetch_array($select_jual)) {
			echo "<tr align='center'>";
			echo "<td>" . $i++ . "</td>";
			echo "<td>" . date('d-m-Y', strtotime($row_jual['tanggal'])) . "</td>";

			$select_user = mysqli_query($con, "SELECT * FROM tb_user WHERE id = $row_jual[id]");
			$row_user = mysqli_fetch_array($select_user);
			echo "<td> $row_user[nama] </td>";

			$select_detailproduk = mysqli_query($con, "SELECT * FROM tb_detailproduk WHERE id = $row_jual[iddetailproduk]");
			$row_detailproduk = mysqli_fetch_array($select_detailproduk);

			$select_produk = mysqli_query($con, "SELECT * FROM tb_produk WHERE id = $row_detailproduk[idproduk]");
			$row_produk = mysqli_fetch_array($select_produk);

			$select_warna = mysqli_query($con, "SELECT * FROM tb_warna WHERE id = $row_detailproduk[idwarna]");
			$row_warna = mysqli_fetch_array($select_warna);

			$select_ukuran = mysqli_query($con, "SELECT * FROM tb_ukuran WHERE id = $row_detailproduk[idukuran]");
			$row_ukuran = mysqli_fetch_array($select_ukuran);

			echo "<td>" . $row_produk['nama'] . " - " . $row_warna['nama'] . " - " . $row_ukuran['nama'] . "</td>";

			echo "<td>" . $row_jual['qty'] . "</td>";
			echo "<td>" . number_format($row_jual['harga']) . "</td>";
			echo "<td>" . number_format($row_jual['biaya']) . "</td>";
			echo "<td>" . number_format($row_jual['diskon']) . "</td>";

			$total = ($row_jual['qty'] * $row_jual['harga']) + $row_jual['biaya'] - $row_jual['diskon'];
			echo "<td>" . number_format($total) . "</td>";

			echo "<td><a href='ubah_jual.php?id=$row_jual[id]'> <i class='fa fa-edit'></i> </a> | <a href='hapus_jual.php?id=$row_jual[id]' onclick='return confirm(\"Anda yakin menghapus data ini?\")'> <i class='fa fa-trash'> </a></td>";
			echo "</tr>";
		}
		?>
	</table>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/typed.js"></script>

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

	<script src="fontawesome/js/all.min.js"></script>
	<script src="fontawesome/js/fontawesome.min.js"></script>
</body>

<footer class="sticky-footer bg-white">
	<marquee behavior="" direction="">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
			</div>
		</div>
	</marquee>
</footer>

</html>