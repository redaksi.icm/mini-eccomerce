<?php
require_once('koneksi.php');
if (isset($_POST['login'])) {
	// $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
	// $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

	$email = $_POST["email"];
	$password = $_POST["password"];

	$select = mysqli_query($con, "SELECT * FROM tb_user WHERE email = '$email' OR password = '$password'");
	// var_dump($select);

	if (mysqli_num_rows($select) > 0) {
		session_start();
		$_SESSION['user'] = mysqli_fetch_array($select);
		// echo ($_SESSION['user']['nama']);
		header("Location: home.php");
	} else {
		echo "<font color=red> Login gagal. </font>";
	}

	// $sql = "SELECT * FROM tb_user WHERE email=:email";
	// $stmt = $db->prepare($sql);

	// bind parameter ke query
	// $params = array(
	// 	":email" => $email
	// );

	// $stmt->execute($params);

	// $user = $stmt->fetch(PDO::FETCH_ASSOC);
	// var_dump($email);

	// jika user terdaftar
	// if ($user) {
	// 	// verfikasi password
	// 	if (password_verify($password, $user['password'])) {
	// 		// buat session
	// 		session_start();
	// 		$_SESSION["user"] = $user;
	// 		// login sukses, alihkan ke home
	// 		header("Location: home.php");
	// 	}
	// }
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ApaanSihKak's SHOP | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="AdminLTE/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="AdminLTE/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="AdminLTE/index2.html"><b>ApaanSihKak's </b>SHOP</a>
		</div>
		<!-- /.login-logo -->
		<div class="card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Sign in to start your session</p>

				<form action="" method="post">
					<div class="input-group mb-3">
						<input type="email" class="form-control" placeholder="Email" name="email">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-envelope"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input type="password" class="form-control" placeholder="Password" name="password">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							<div class="icheck-primary">
								<input type="checkbox" id="remember">
								<label for="remember">
									Remember Me
								</label>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-4">
							<button type="submit" class="btn btn-primary btn-block" name="login">Sign In</button>
						</div>
						<!-- /.col -->
					</div>
				</form>

				<p class="mb-0">
					<a href="register.php" class="text-center">Register a new membership</a>
				</p>
			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="AdminLTE/dist/js/adminlte.min.js"></script>

</body>

</html>