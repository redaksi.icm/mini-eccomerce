<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
    $select = mysqli_query($con, "SELECT * FROM tb_detailproduk WHERE id = " . $_GET['id']);
    $row = mysqli_fetch_array($select);
    $idproduk = $row["idproduk"];
    $idwarna = $row["idwarna"];
    $idukuran = $row["idukuran"];
    $stok = $row["stok"];
}

if (isset($_POST['submit'])) {
    $idproduk = $_POST["idproduk"];
    $idwarna = $_POST["idwarna"];
    $idukuran = $_POST["idukuran"];
    $stok = $_POST["stok"];

    $update = mysqli_query($con, "UPDATE tb_detailproduk SET idproduk = $idproduk, idwarna = $idwarna, idukuran = $idukuran, stok = $stok WHERE id = " . $_GET['id']);
    if ($update) {
        echo "<font color=green> Data Berhasil Diubah </font>";
    } else {
        echo "<font color=red> Data Gagal Diubah </font>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Detail Produk</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</head>

<body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="">ApaanSihKak's Shop</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="cart.php"> Cart </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="order.php"> Order </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="warna.php"> Warna </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="ukuran.php"> Ukuran </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_produk.php"> Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="lihat_detailproduk.php"> Detail Produk </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="about.php"> About Me </a>
            </li>
        </ul>
    </nav>

    <div class="col-md-12 offset-md-3">
        <div class="col-6">
            <center>
                <h1>Ubah Detail Produk</h1>
            </center>
            <hr>
        </div>
        <br>

        <div class="container">
            <div class="row">
                <div class="col-md-3 offset-md-5">
                    <a href="lihat_detailproduk.php">Kembali</a>
                </div>
            </div>
        </div>

        <br>

        <form name="form1" method="POST" action="">
            <div class="form-group row">
                <label for="idproduk" class="col-sm-2 col-form-label"> Produk </label>
                <div class="col-sm-4">
                    <select class="form-control" id="idproduk" name="idproduk">
                        <?php
                        // memasukkan semua data yang ada di tb_produk ke dalam Combo Box.
                        $select_produk = mysqli_query($con, "SELECT * FROM tb_produk");
                        while ($row_produk = mysqli_fetch_array($select_produk)) {
                            if ($idproduk == $row_produk['id']) // $idproduk didapat dari atas (baris 7)
                            {
                                echo "<option value=$row_produk[id] selected> $row_produk[nama] </option>";
                            } else {
                                echo "<option value=$row_produk[id]> $row_produk[nama] </option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="idwarna" class="col-sm-2 col-form-label"> Warna </label>
                <div class="col-sm-2">
                    <select class="form-control" id="idwarna" name="idwarna">
                        <?php
                        // memasukkan semua data yang ada di tb_warna ke dalam Combo Box.
                        $select_warna = mysqli_query($con, "SELECT * FROM tb_warna");
                        while ($row_warna = mysqli_fetch_array($select_warna)) {
                            if ($idwarna == $row_warna['id']) // $idwarna didapat dari atas (baris 8)
                            {
                                echo "<option value=$row_warna[id] selected> $row_warna[nama] </option>";
                            } else {
                                echo "<option value=$row_warna[id]> $row_warna[nama] </option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="idukuran" class="col-sm-2 col-form-label"> Ukuran </label>
                <div class="col-sm-1">
                    <select class="form-control" id="idukuran" name="idukuran">
                        <?php
                        // memasukkan semua data yang ada di tb_ukuran ke dalam Combo Box.
                        $select_ukuran = mysqli_query($con, "SELECT * FROM tb_ukuran");
                        while ($row_ukuran = mysqli_fetch_array($select_ukuran)) {
                            if ($idukuran == $row_ukuran['id']) // $idukuran didapat dari atas (baris 9)
                            {
                                echo "<option value=$row_ukuran[id] selected> $row_ukuran[nama] </option>";
                            } else {
                                echo "<option value=$row_ukuran[id]> $row_ukuran[nama] </option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="stok" class="col-sm-2 col-form-label"> Stok </label>
                <div class="col-sm-2">
                    <input type="number" class="form-control" name="stok" value="<?= $stok; ?>" min="0" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-1">
                    <button class="btn btn-primary" type="submit" name="submit">Ubah</button>
                </div>
            </div>

        </form>
    </div>

    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/typed.js"></script>
</body>

<footer class="sticky-footer bg-white" style="position: absolute; bottom: 0; width: 100%; height: 30px;">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; willywin99's Web Programming <?= date('Y'); ?></span>
        </div>
    </div>
</footer>

</html>