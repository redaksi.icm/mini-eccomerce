<?php
require_once('koneksi.php');

if (isset($_GET['id'])) {
    $delete = mysqli_query($con, "DELETE FROM tb_supplier WHERE id = " . $_GET['id']);
    if ($delete) {
        echo "<font color=green> Supplier Berhasil Dihapus </font>";
    } else {
        echo "<font color=red> Supplier Gagal Dihapus </font>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hapus Supplier</title>
</head>

<body>
    <a href="lihat_supplier.php">Kembali</a>
</body>

</html>